class Coordenada:
    def __init__(self, long, lat):
        self.longitud = long
        self.latitud = lat
        
    def __str__(self):
        return "La latitud es: {lat} y la longitud es: {long}".format(lat = self.latitud, long = self.longitud)
        
coordenadasMadrid = Coordenada(222,111)
coordenadasGijon = Coordenada(444,333)

class Ciudad:
    def __init__(self, n, h, c):
        self.nombre = n
        self.habitantes = h
        self.coordenada = c
        
    def __str__(self):
        return "La ciudad es {name} y tiene {hab} habitantes situada en ({long},{lat})".format(name = self.nombre, hab = self.habitantes, long = self.coordenada.longitud, lat = self.coordenada.latitud)
        
class Aeropuerto:
    def __init__(self, n, a, p, c):
        self.nombre = n
        self.altitud = a
        self.num_pistas = p
        self.coordenada = c
        
    def __str__(self):
        return "La aeropuerto es {name} situado en ({long},{lat}) y tiene {alt}m de altura con {num} pistas".format(name = self.nombre, alt = self.altitud, num = self.num_pistas ,long = self.coordenada.longitud, lat = self.coordenada.latitud)
        

        
ciudadMadrid = Ciudad("Madrid", 3000000, coordenadasMadrid)
ciudadGijon = Ciudad("Gijón", 200000, coordenadasGijon)

aeropuertoBarajas = Aeropuerto("Barajas", 120, 18, coordenadasMadrid)

print(coordenadasMadrid)
print(ciudadMadrid)
print(aeropuertoBarajas)